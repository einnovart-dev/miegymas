set :application, "miegymascafe.hu"
set :domain,      "213.181.208.130"
set :user,        "root"
set :deploy_to,   "/var/www/html/miegymas"

role :app, domain
role :web, domain

set :scm,         :git
set :repository,  "git@bitbucket.org:einnovart-dev/miegymas.git"
set :deploy_via, :remote_cache

set :use_composer, false
set :update_vendors, false

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :shared_files,          ["wp-config.php"]
set :shared_children,       ["wp-content/uploads"]

set  :use_sudo,      false
set  :keep_releases,  3

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

set :interactive_mode, false

namespace :myproject do
    task :symlink, :roles => :app do
        run "ln -nfs #{shared_path}/wp-config.php #{release_path}/wp-config.php"
    end
end

after "deploy:create_symlink", "myproject:symlink"
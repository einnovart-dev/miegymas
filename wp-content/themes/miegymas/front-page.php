<?php get_header(); ?>

    <section class="hero block">
        <span class="overlay-pattern"></span>

        <div class="hero-text-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                        <div class="intro text-center">
                            <img src="/wp-includes/images/logo.jpg" alt="">
                            <h1>Miegymás cafe&more</h1>
                            <p>
                                Tisztelt Látogató,

                                <br>
                                a Miegymás cafe&more honlapjának kialakítása folyamatban van,<br>
                                addig is kérjük látogasson el a <a href="https://www.facebook.com/miegymascafe">facebook
                                    oldalunkra</a>!
                                <br>
                                <br>
                                Várjuk szeretettel!
                                <br>
                                Címünk: <a href="https://goo.gl/maps/hdTPvAmpDWK2" target="_blank">Budapest, V. kerület,
                                    Arany János utca 16.</a>
                                <br><br>
                            </p>

                            NYITVATARTÁS<br>

                            <div class="row">
                                <div class="col-xs-6 text-right">
                                    Hétfő-szerda:

                                </div>
                                <div class="col-xs-6 text-left">
                                    8:00 - 19:00
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 text-right">
                                    Csütörtök-péntek:

                                </div>
                                <div class="col-xs-6 text-left">
                                    8:00 - 20.00
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 text-right">
                                    Szombat:

                                </div>
                                <div class="col-xs-6 text-left">
                                    9:00 - 18.00
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 text-right">
                                    Vasárnap:
                                </div>
                                <div class="col-xs-6 text-left">
                                    9:00 - 18.00
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?php wp_title(); ?></title>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700,800&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>

    <?php wp_head(); ?>
</head>

<body <?php body_class(isset($class) ? $class : ''); ?>>
